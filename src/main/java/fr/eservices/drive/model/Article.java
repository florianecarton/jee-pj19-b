package fr.eservices.drive.model;

import javax.persistence.*;

import com.sun.istack.Nullable;

@Entity
@Table(name="article")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Article {
	
	@Id
	@Column(name="reference")
	private String id ;

	@Column(name="name")
	private String name;
	@Nullable
	@Column(name="img")
	private String img;
	
	@Column
	private String categorie;
	
	@Column(name="price")
	private int price;
	@Column(name="stock")
	private int stock ; 
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	public String getCategorie() {
		return categorie;
	}
	
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	
}
