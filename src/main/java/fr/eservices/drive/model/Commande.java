package fr.eservices.drive.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Commande {

	String reference;
	List<Article> articles = new ArrayList<>();
	LocalDate dateCommande;
	LocalDate dateRetrait;	
		
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public LocalDate getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(LocalDate dateCommande) {
		this.dateCommande = dateCommande;
	}
	public LocalDate getDateRetrait() {
		return dateRetrait;
	}
	public void setDateRetrait(LocalDate dateRetrait) {
		this.dateRetrait = dateRetrait;
	}
	
	
}
