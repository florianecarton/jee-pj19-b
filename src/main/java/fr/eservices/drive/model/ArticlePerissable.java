package fr.eservices.drive.model;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import java.util.*;

@Entity
@Table(name="article_perissable")
public class ArticlePerissable{
	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="date")
	private Date date_limite; 
	@Id
	@GeneratedValue
	@Column(name="lot")
	private int lot ; 
	@Column(name="quantite_recu")
    private int quantite_recu ;
    
    @ManyToOne(optional=false)
    @NotNull
    @JoinColumn(name = "reference")
    private Article article;
    
    @Column(name="stock_p")
    private int stock_p;
    
    
    public int getStock_p() {
		return stock_p;
	}
	public void setStock_p(int stock_p) {
		this.stock_p = stock_p;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Date getDate_limite() {
		return date_limite;
	}
	public void setDate_limite(Date date_limite) {
		this.date_limite = date_limite;
	}
	public int getLot() {
		return lot;
	}
	public void setLot(int lot) {
		this.lot = lot;
	}
	public int getQuantite_recu() {
		return quantite_recu;
	}
	public void setQuantite_recu(int quantite_recu) {
		this.quantite_recu = quantite_recu;
	}
}
