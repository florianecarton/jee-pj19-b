package fr.eservices.drive.model;

import java.util.ArrayList;
import java.util.List;

public class Categorie {
	
	  private List<String> categories;
	   
	  public Categorie(){
		this.categories = new ArrayList<String>();
	    this.categories.add("Fruits");
	    this.categories.add("Légumes");
	    this.categories.add("Conserves");
	    this.categories.add("Droguerie");
	    this.categories.add("Fromage");
	    this.categories.add("Biscuits");
	    this.categories.add("Céréales");
	    this.categories.add("Viandes");
	    this.categories.add("Boissons");
	    this.categories.add("Confiserie");

	    
	  }
	   
	  public List<String> getAll(){
	    return categories;
	  }
	}