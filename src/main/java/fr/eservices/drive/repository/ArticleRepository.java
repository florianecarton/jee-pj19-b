package fr.eservices.drive.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import fr.eservices.drive.dao.ArticleDao;
import fr.eservices.drive.dao.OrderDao;
import fr.eservices.drive.model.Article;

@Repository
public interface ArticleRepository extends ArticleDao ,CrudRepository <Article,String> {
	
	Article findById(String id);
	Page<Article> findAll(Pageable p);
	Page<Article> findByCategorie(String Categorie , Pageable p);
	Page<Article> findById(String id , Pageable p);
	Page<Article> findByName(String name , Pageable p);
	//@Query("SELECT DISTINCT(a.categorie) FROM Article a order by a.categorie")
	//List<String> selectDistinctCategorie();
	@Query("SELECT a FROM Article a WHERE a.id = ?1 OR a.name=?2 OR a.categorie=?3")
	Page<Article> findArticlesAllCri(String id, String name , String categorie , Pageable pageable);
	@Query("SELECT a FROM Article a WHERE a.id = ?1 OR a.name=?2")
	Page<Article> findArticlesNameId(String id, String name , Pageable pageable);
	@Query("SELECT a FROM Article a WHERE  a.name=?1 OR a.categorie=?2")
	Page<Article> findArticlesNameCateg( String name , String categorie , Pageable pageable);
	@Query("SELECT a FROM Article a WHERE a.id = ?1 OR a.categorie=?2")
	Page<Article> findArticlesIdCateg(String id, String categorie , Pageable pageable);



}
