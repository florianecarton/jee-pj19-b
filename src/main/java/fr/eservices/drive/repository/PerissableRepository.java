package fr.eservices.drive.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.eservices.drive.dao.PerissableDao;
import fr.eservices.drive.model.ArticlePerissable;



@Repository
public interface PerissableRepository extends PerissableDao, CrudRepository <ArticlePerissable,String>
{

	ArticlePerissable findByArticle_IdAndLot(String id, int lot);
	

}