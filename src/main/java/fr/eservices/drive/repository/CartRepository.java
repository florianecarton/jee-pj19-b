package fr.eservices.drive.repository;

import org.springframework.stereotype.Repository;

import fr.eservices.drive.dao.CartDao;

@Repository
public interface CartRepository 
extends CartDao
{

}
