package fr.eservices.drive.web;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import java.util.List;
import java.util.Random;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.PerissableRepository;
import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.ArticlePerissable;
import fr.eservices.drive.model.Commande;

@Controller
public class CommandeController {

	@Autowired
	ArticleRepository articleRepo;

	@Autowired
	private PerissableRepository perssibaleRepo;

	//@Autowired
	//private CommandeRepository repoCommande;

	@RequestMapping(path = "produits.html", method = RequestMethod.GET)
	public String listProduits(Model model, HttpSession session) {

		// Recuperer les perissables dont la date limit < 5
		LocalDate date = LocalDate.now().plusDays(5);

		List<ArticlePerissable> articlesPerisable = (List<ArticlePerissable>) perssibaleRepo.findAll();
		// recuperer tous les artciles depuis parrisable
		List<Article> articlesPerisable2 = articlesPerisable.stream()
//				.filter(p -> p.getDate_limite().isAfter(date))
				.map(p -> p.getArticle()).collect(Collectors.toList());

		// Recuperer les articles avec le stock > 0 dans la BDD
		List<Article> articles = (List<Article>) articleRepo.findAll();
		List<Article> articles2 = articles.stream()
				.filter(p -> p.getStock() > 0)
				.collect(Collectors.toList());

		List<Article> result = articles2.stream().distinct().filter(articlesPerisable2::contains)
				.collect(Collectors.toList());

		model.addAttribute("list_produits", articles2);
		// mettre les articles parissables et non parrisable dans une list

		return "produit-list";
	}

	@RequestMapping(path = "/commanderProduit.html")
	public String commander(HttpSession session, Model model) {
		List<Article> list = (List<Article>) session.getAttribute("commande");
		Commande commande = new Commande();
		commande.setArticles(list);
		commande.setDateCommande(LocalDate.now());
		commande.setReference(String.valueOf(1 + new Random().nextInt(1000 - 1 + 1)));
		commande.setDateRetrait(LocalDate.now().plusDays(23));
		//repoCommande.save(commande);
		model.addAttribute("commande", commande);
		session.removeAttribute("commande");
		session.removeAttribute("nbcommande");
		

		return "commande";

	}
	
	
	@RequestMapping(path = "/annulerCommande.html")
	public String annulerCommande(HttpSession session, Model model) {
		List<Article> list = (List<Article>) session.getAttribute("commande");
		List<Integer> listNb = (List<Integer>) session.getAttribute("nbcommande");

		for (int i = 0; i < list.size(); i += 1) {
			list.get(i).setStock(list.get(i).getStock() + listNb.get(i));
			articleRepo.save(list.get(i));
		}
		session.removeAttribute("commande");
		session.removeAttribute("nbcommande");
		

		return "redirect:/produits.html";

	}
	

	@RequestMapping(path = "/panier.html", method = RequestMethod.POST)
	public String addtopanel(Model model, @RequestParam String id, @RequestParam(defaultValue = "1") int nb, HttpSession session) {

		if (session.getAttribute("commande") == null) {
			session.setAttribute("commande", new ArrayList<Article>());
		}
		if (session.getAttribute("nbcommande") == null) {
			session.setAttribute("nbcommande", new ArrayList<Integer>());
		}

		List<Article> list = (List<Article>) session.getAttribute("commande");
		List<Integer> listNb = (List<Integer>) session.getAttribute("nbcommande");
		Article article = articleRepo.findById(id);
		list.add(article);
		listNb.add(nb);
		session.setAttribute("commande", list);
		session.setAttribute("nbcommande", listNb);

		article.setStock(article.getStock() - nb);
		articleRepo.save(article);

		return "redirect:/produits.html";
	}

}
