package fr.eservices.drive.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.ArticlePerissable;
import fr.eservices.drive.model.Categorie;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.PerissableRepository;


@Controller
public class ArticleController {
	
	@Autowired
	ArticleRepository articleRepo ; 
	
	
	@RequestMapping(path="/article/{id}")
	public String findArticle(@PathVariable String id, Model model) {
		
		Article article = articleRepo.findById(id);
		model.addAttribute("article", article);
        return "article_view";
	}
	
	@RequestMapping(path="article.html" , method = RequestMethod.GET)
	public String list(Model model,
			@RequestParam(defaultValue="1") int page,
			@RequestParam(defaultValue="") String  categorie,
			@RequestParam(defaultValue="") String  id,
			@RequestParam(defaultValue="") String  name ,
			@RequestParam(defaultValue="10") int  pagination){
		int size = pagination ;
		page -- ;
		PageRequest pageable =  new PageRequest(page, size);
		Page<Article> pageArticles ; 
				if((categorie==null || categorie.isEmpty()) && (id==null || id.isEmpty()) && (name==null || name.isEmpty())) {
					pageArticles=articleRepo.findAll(pageable) ;
				}
				else if((categorie==null || categorie.isEmpty()) && (id==null || id.isEmpty())) {
					pageArticles=articleRepo.findByName( name , pageable);
				}
                else if((categorie==null || categorie.isEmpty()) && (name==null || name.isEmpty())) {
                	pageArticles=articleRepo.findById( id , pageable );
				}
               else if((id==null || id.isEmpty()) && (name==null || name.isEmpty())) {
            	   pageArticles=articleRepo.findByCategorie(categorie,pageable); 
				}
               else if((categorie==null || categorie.isEmpty()) ) {
            	   pageArticles=articleRepo.findArticlesNameId( id,  name ,  pageable);
				}
               else if((name==null || name.isEmpty()) ) {
            	   pageArticles=articleRepo.findArticlesIdCateg( id,  categorie ,  pageable);
				}
               else if((id==null || id.isEmpty()) ) {
            	   pageArticles=articleRepo.findArticlesNameCateg(  name ,  categorie ,  pageable);
				}
				else {
					pageArticles=articleRepo.findArticlesAllCri( id,  name ,  categorie ,  pageable);
				}
				Categorie cat = new Categorie();
				List<String> categories = cat.getAll();
		model.addAttribute("list_article", pageArticles);
		model.addAttribute("categorie", categories);
        return "article";
	}
	
	@RequestMapping(path="article/new.html", method = RequestMethod.GET)
	private String createForm(Model model){
		Article article = new Article();
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("article",article);
		model.addAttribute("categorie", categories);
		return "article_form";
	}
	
	@RequestMapping(path="article/new.html", method= RequestMethod.POST)
	private String addArticle (Model model, @ModelAttribute("article") Article article ) {

		String erreurId ="";
		String erreurName ="";
		String erreurPrice ="";
		String erreurCategorie ="";
		String erreurStock ="";
		if("".equals(article.getId())) {
			erreurId="Vous devez entrer une reference.";
		}
		else {
			Article findArticle = articleRepo.findById(article.getId());
			if(findArticle != null) erreurId="cette reference existe en base.";
		}
		if("".equals(article.getName())){
			erreurName = " Vous devez entrer un nom.";
		}
		if(article.getPrice()<0) erreurPrice = " Vous devez entrer un prix positif.";
		if(article.getStock()<0) erreurStock = "Vous devez entrer un stock positif";
		
		if(erreurId=="" && erreurName =="" && erreurPrice =="" && erreurCategorie =="" && erreurStock =="") {
			articleRepo.save(article);
			return "redirect:/article.html";
		}
		else {
			model.addAttribute("erreurId",erreurId);
			model.addAttribute("erreurName",erreurName);
			model.addAttribute("erreurPrice",erreurPrice);
			model.addAttribute("erreurCategorie",erreurCategorie);
			model.addAttribute("erreurStock",erreurStock);
			Categorie cat = new Categorie();
			List<String> categories = cat.getAll();
			model.addAttribute("article",article);
			model.addAttribute("categorie", categories);
			return "article_form";
		}

	}
	
	@RequestMapping(path="article/edit/{id}.html", method = RequestMethod.GET)
	private String findArticleEdit(Model model, @PathVariable(name="id") String id){
		Article article = articleRepo.findById(id);
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("article",article);
		model.addAttribute("categorie", categories);
		model.addAttribute("id",id);
		return "article_form_edit";
	}
	
	@RequestMapping(path="article/edit/{id}.html", method= RequestMethod.POST)
	private String update (Model model, @ModelAttribute("article") Article article, @PathVariable(name="id") String id ) {
		
		String erreurName ="";
		String erreurPrice ="";
		String erreurCategorie ="";
		String erreurStock ="";
		if("".equals(article.getName())){
			erreurName = " Vous devez entrer un nom.";
		}
		if(article.getPrice()<0) erreurPrice = " Vous devez entrer un prix positif.";
		if(article.getStock()<0) erreurStock = "Vous devez entrer un stock positif";
		if(erreurName =="" && erreurPrice =="" && erreurCategorie =="" && erreurStock =="") {
			PageRequest pageable =  new PageRequest(1, 10);
			Article articleUpdate = articleRepo.findById(id);
			articleUpdate.setName(article.getName());
			articleUpdate.setPrice(article.getPrice());
			articleUpdate.setCategorie(article.getCategorie());
			articleUpdate.setStock(article.getStock());
			articleRepo.save(articleUpdate);
			Page<Article> pageArticles =articleRepo.findAll(pageable) ;
			model.addAttribute("list_article",pageArticles);
			return "redirect:/article.html";
		}
		else {
			model.addAttribute("erreurName",erreurName);
			model.addAttribute("erreurPrice",erreurPrice);
			model.addAttribute("erreurCategorie",erreurCategorie);
			model.addAttribute("erreurStock",erreurStock);
			Categorie cat = new Categorie();
			List<String> categories = cat.getAll();
			model.addAttribute("article",article);
			model.addAttribute("categorie", categories);
			return "article_form_edit";
		}
		    
		}
	
	

}
