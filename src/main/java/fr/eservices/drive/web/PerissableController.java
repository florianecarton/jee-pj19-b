package fr.eservices.drive.web;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.ArticlePerissable;
import fr.eservices.drive.model.Categorie;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.PerissableRepository;


@Controller
public class PerissableController {

	@Autowired
	private PerissableRepository repo;
	
	@Autowired
	private ArticleRepository articleRepo;
	
	@RequestMapping(path = "perissable.html", method = RequestMethod.GET)
	private String listPerissable(Model model){
		// Recuperer les perissables dans la BDD
		List<ArticlePerissable> articles = (List<ArticlePerissable>) repo.findAll();
		model.addAttribute("articles",articles);
		return "perissable";
	}
	
	@RequestMapping(path="perissable/new.html", method = RequestMethod.GET)
	private String openForm(Model model){
		ArticlePerissable article = new ArticlePerissable();
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("categories",categories);
		model.addAttribute("article_perissable",article);
		model.addAttribute("mode","new");
		return "perissable_form";
	}
	
	@RequestMapping(path="perissable/new.html", method= RequestMethod.POST)
	private String savePerissable (Model model, @ModelAttribute("article_perissable") ArticlePerissable article ) {
		if(validateForm(model,article)) {
			//le formulaire est correcte, on peut save l'article
			Article articleBase = article.getArticle();
			article.setStock_p(article.getQuantite_recu());
			if(!articleRepo.exists(articleBase.getId())) {
				articleBase.setStock(article.getStock_p());
				articleRepo.save(articleBase);
			}
			else{
				Article art = articleRepo.findById(articleBase.getId());
				articleBase.setStock(art.getStock()+article.getStock_p());
				articleRepo.save(articleBase);
			}
			repo.save(article);
			model.addAttribute("articles",(List<ArticlePerissable>)repo.findAll());
			return "perissable";
		}
		// Une erreur a �t� trouv�, on reste sur le formulaire
		model.addAttribute("mode","new");
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("categories",categories);
		return "perissable_form";
	}
	
	private boolean validateForm(Model model, ArticlePerissable article) {
		String messageErreur = "";
		if("".equals(article.getArticle().getId())) {
			messageErreur += "- Vous devez entrer une r�f�rence. \n";
		}
		// TODO : when BDD, check que l'id n'existe pas en base.
		if("".equals(article.getArticle().getName())){
			messageErreur += " - Vous devez entrer un nom. \n";
		}

		if(article.getArticle().getPrice()<0) messageErreur += " Vous devez entrer un prix positif.";
		if(article.getQuantite_recu()<0) messageErreur += " Vous devez entrer une quantit&eacute; positive.";
		if(article.getLot()<0) messageErreur += "Vous devez entrer un lot positif";
	
		/*
		messageErreur += " "+article.getArticle().getId()
					  +" "+article.getLot()
					  +" "+article.getArticle().getName()
					  +" "+article.getArticle().getPrice()
					  +" "+article.getQuantite_recu()
					  +" "+article.getArticle().getStock()
					  +" "+article.getDate_limite();
					  */
		//messageErreur += article.getDate_limite();
		model.addAttribute("messageErreur",messageErreur);
		if("".equals(messageErreur)) return true;
		return false;
	}
	
	@RequestMapping(path="perissable/edit/{id}/{lot}.html", method = RequestMethod.GET)
	private String getArticleForEdit(Model model,
			@PathVariable(name="id") String id,
			@PathVariable(name="lot") int lot){
		ArticlePerissable article = repo.findByArticle_IdAndLot(id, lot);
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("categories",categories);
		model.addAttribute("article_perissable",article);
		model.addAttribute("mode","edit");
		model.addAttribute("id",id);
		model.addAttribute("lot",lot);
		return "perissable_form";
	}
	
	@RequestMapping(path="perissable/edit/{id}/{lot}.html", method= RequestMethod.POST)
	private String update (Model model, @ModelAttribute("article_perissable") ArticlePerissable article,
			@PathVariable(name="id") String id,
			@PathVariable(name="lot") int lot ) {
		if(validateForm(model,article)) {
			//le formulaire est correcte, on peut update l'article
			ArticlePerissable artEnBDD = repo.findByArticle_IdAndLot(id, lot);
			if(artEnBDD.getStock_p() > article.getStock_p()) {
				int diff = artEnBDD.getStock_p() - article.getStock_p();
				article.getArticle().setStock(artEnBDD.getArticle().getStock()-diff);
			}
			else if(artEnBDD.getStock_p() < article.getStock_p()) {
				int diff = article.getStock_p() - artEnBDD.getStock_p();
				article.getArticle().setStock(artEnBDD.getArticle().getStock()+diff);
			}
			article.getArticle().setId(id);
			article.setLot(lot);
			articleRepo.save(article.getArticle());
			repo.save(article);
			model.addAttribute("articles",(List<ArticlePerissable>)repo.findAll());
			return "perissable";
		}
		// Une erreur a �t� trouv�, on reste sur le formulaire
		model.addAttribute("mode","edit");
		Categorie cat = new Categorie();
		List<String> categories = cat.getAll();
		model.addAttribute("categories",categories);
		return "perissable_form";
	}
}
