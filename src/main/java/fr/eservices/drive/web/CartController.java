package fr.eservices.drive.web;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.eservices.drive.dao.ArticleDao;
import fr.eservices.drive.dao.CartDao;
import fr.eservices.drive.dao.DataException;
import fr.eservices.drive.dao.OrderDao;
import fr.eservices.drive.mock.ArticleMockDao;
import fr.eservices.drive.mock.CartMockDao;
import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Cart;
import fr.eservices.drive.model.Order;
import fr.eservices.drive.repository.ArticleRepository;
import fr.eservices.drive.repository.CartRepository;
import fr.eservices.drive.repository.OrderRepository;
import fr.eservices.drive.web.dto.CartEntry;
import fr.eservices.drive.web.dto.SimpleResponse;
import fr.eservices.drive.web.dto.SimpleResponse.Status;


@Controller
@RequestMapping(path="/cart")
public class CartController {
	
	@Autowired CartMockDao daoCartMock;
	
	@Autowired
	ArticleRepository articleRepository;
	
	@Autowired
	OrderRepository repo;
	
	@Autowired
	CartDao dao;

	
	@ExceptionHandler(DataException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public String dataExceptionHandler(Exception ex) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintWriter w = new PrintWriter( out );
		ex.printStackTrace(w);
		w.close();
		return 
			"ERROR"
			+ "<!--\n" + out.toString() + "\n-->";
	}
	
	@GetMapping(path="/{id}.html", produces="text/html")
	public String getCart(@PathVariable(name="id") int id, Model model) throws DataException {
		if(id<=0){
			throw new DataException("L'id doit être supérieur à 0");
		}
		// get cart from dao
		Cart cart = this.daoCartMock.getCartContent(id);
		// assign to model var "cart"
		model.addAttribute("cart", cart);
		// return view name to display content of /WEB-INF/views/_cart_header.jsp
		return "_cart_header";
	}

	@ResponseBody
	@PostMapping(path="/{id}/add.json",consumes="application/json")
	public SimpleResponse add(@PathVariable(name="id") int id, @RequestBody CartEntry art) throws DataException {
		SimpleResponse res = new SimpleResponse();
		Cart cart ;
		int quantityToAdd;
		Article article;
		List<Article> mesArticles;
		
		
		if(this.daoCartMock.getCartContent(id)==null){
			this.daoCartMock.store(id, new Cart());
		}
		cart = this.daoCartMock.getCartContent(id);
		mesArticles = cart.getArticles();
			
		// Check que l'article existe
		article = this.articleRepository.findById(art.getId());
		if(article == null){
			res.status = Status.ERROR;
			res.message = "L'article n'existe pas";
			return res;
		}
				
		// Check que la quantité soit positive ou null
		quantityToAdd = art.getQty();
		if (quantityToAdd <0){
			res.status = Status.ERROR;
			res.message = "La quantité est négative";
			return res;
		}
		
		for(int index=0; index<quantityToAdd; index++){
			mesArticles.add(article);
		}
		
		cart.setArticles(mesArticles);
		
		/*
		System.out.println(
			"********************\n"
			+ "***** " + String.format("Add Article %d x [%s] to cart", art.getQty(), art.getId()) + "\n" 
			+ "********************"
		);
		*/
		
		res.status = Status.OK;
		res.message = "Les articles ont été ajoutés";
		return res;
		
	}
	
	@RequestMapping("/{id}/validate.html")
	public String validateCart(@PathVariable(name="id") int id, Model model) throws DataException {
		
		// get cart by its id
		if(this.daoCartMock.getCartContent(id)==null){
			throw new DataException("Le cart n'existe pas");
		}
		Cart cart = this.daoCartMock.getCartContent(id);
	
		// create an order
		Order order = new Order();
		// for each article, add it to the order
		List<String> articlesToOrder = new ArrayList<String>();
		int amount = 0;
		for(Article article : cart.getArticles()){
			articlesToOrder.add(article.getId());
			amount = amount + article.getPrice();
		}
		// set order date
		order.setCreatedOn(new Date());
		// set order amount (sum of each articles' price)
		order.setAmount(amount);
		order.setCustomerId("chuckNorris");
		order.setCurrentStatus(fr.eservices.drive.dao.Status.ORDERED);
		this.repo.save(order);
		// persist everything
		// redirect user to list of orders
		
		return "order_list";
	}
}
