//package fr.eservices.drive.mock;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Component;
//
//import fr.eservices.drive.dao.CommandeDao;
//import fr.eservices.drive.dao.DataException;
//import fr.eservices.drive.model.Article;
//import fr.eservices.drive.model.Commande;
//
//@Component
//@Qualifier("mock")
//public class CommandeMockDao implements CommandeDao{
//	
//	@Autowired
//	ArticleMockDao articleDao;
//	
//	private List<Commande> commandes = new ArrayList<Commande>();
//	
//	public CommandeMockDao(){
//		Commande commande1 = new Commande();
//		
//		// ARTICLES (en dur pour le moment)
//		List<Article> articles = new ArrayList<Article>();
//		
//		Article a = new Article();
//		a.setId("10101010");
//		a.setName("Boisson énergétique");
//		a.setPrice(299);
//		a.setImg("https://static1.chronodrive.com/img/PM/P/0/76/0P_61276.gif");
//		articles.add( a );
//	
//	
//	    Article b = new Article();
//		b.setId("10101012");
//		b.setName("Papier Cadeau");
//		b.setPrice(150);
//		b.setImg("https://static1.chronodrive.com/img/PM/P/0/72/0P_348972.gif");
//		articles.add(b);
//		
//		commande1.setArticles(articles);
//		commande1.setReference("1");
//		commande1.setDateCommande(new Date());
//		commande1.setDateCommande(new Date());
//		
//		commandes.add(commande1);
//		
//		
//		Commande commande2 = new Commande();
//		
//		// ARTICLES (en dur pour le moment)
//		List<Article> articles2 = new ArrayList<Article>();
//		articles2.add(a);
//		commande2.setArticles(articles);
//		commande2.setReference("2");
//		commande2.setDateCommande(new Date());
//		commande2.setDateCommande(new Date());
//		commandes.add(commande2);
//	}
//	
//
//	@Override
//	public List<Commande> getCommandes() throws DataException {
//		return commandes;
//	}
//
//}
