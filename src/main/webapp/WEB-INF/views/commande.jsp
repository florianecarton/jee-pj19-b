<%@page import="fr.eservices.drive.model.Article"%>
<%@page import="java.util.List"%>
<%@page import="java.time.LocalDate"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="./_header.jsp"%>

<h2>Detail de votre commande</h2>
<p>R�ference : ${ commande.reference}</p>
<p>date de commande : ${ commande.dateCommande}</p>
<p>date de retrait : ${ commande.dateRetrait}</p>

<h3>Liste des articles</h3>

<div class="container">
	<h2>Votre commande</h2>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">Nom</th>
					<th scope="col">Prix</th>
					<th scope="col">Categorie</th>

				</tr>
			</thead>

			<tbody>
  <c:forEach var="articles" items='${ commande.articles }'>
  	  <tr>
		<td>${ articles.name }</td>
		<td>${ articles.price }</td>
		<td>${ articles.categorie }</td>
		

  </tr>
  </c:forEach>

			</tbody>
		</table>
	</div>

</div>