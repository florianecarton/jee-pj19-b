<%@page import="java.util.ArrayList"%>
<%@page import="fr.eservices.drive.model.Article"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Enumeration"%>


<%
	List<Article> list = new ArrayList<Article>();
	List<Integer> listNb = new ArrayList<Integer>();
	if (session.getAttribute("commande") != null && session.getAttribute("nbcommande") != null) {
		list = (List<Article>) session.getAttribute("commande");
		listNb = (List<Integer>) session.getAttribute("nbcommande");
	}
%>

<div class="container">
	<h2>Votre commande</h2>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">Nom</th>
					<th scope="col">Prix</th>
					<th scope="col">Categorie</th>
					<th scope="col">Nombre articles</th>
				</tr>
			</thead>

			<tbody>
				<%
					for (int i = 0; i < list.size(); i += 1) {
				%>
				<tr>
					<td><%=list.get(i).getName()%></td>
					<td><%=list.get(i).getPrice()%></td>
					<td><%=listNb.get(i)%></td>
					<td><%=list.get(i).getCategorie()%></td>
				</tr>
				<%
					}
				%>

			</tbody>
		</table>
	</div>

</div>