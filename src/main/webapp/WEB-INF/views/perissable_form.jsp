<%@include file="./_header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="col-md-6 col-md-offset-3">
<c:set var="modeVar" value="${ mode }" scope="page" />

<c:choose>
 <c:when test="${modeVar == 'new'}">
 	<c:set var="action" value="/stock/perissable/new.html" scope="page" />
 </c:when>
 
 <c:otherwise>
 	<c:set var="action" value="/stock/perissable/edit/${ id }/${ lot }.html" scope="page" />
 </c:otherwise>
 
 </c:choose>
 
 
 <c:choose>
 	<c:when test="${modeVar == 'new'}">
 		<h2> Cr&eacute;er un article p&eacute;rissable </h2>
 	</c:when>
 	<c:otherwise>
 	 <h2> Editer un article p&eacute;rissable </h2>
 	</c:otherwise>
 </c:choose>
 
 
 <form:form action="${ action }" method="post" modelAttribute="article_perissable">
  <div class="form-group">
    <form:label path="article.id">R&eacute;f&eacute;rence</form:label>
	
	<c:choose>
	<c:when test="${modeVar == 'edit'}">
	   <form:input type="text" class="form-control" path="article.id" value="${ article.reference }" disabled="true" />
	</c:when>
	
	<c:otherwise>
	      <form:input type="text" class="form-control" path="article.id" value="${ article.reference }"/>
	</c:otherwise>
	</c:choose>    
    
  </div>

  <div class="form-group">
    <form:label path="article.name">Nom</form:label>
    <form:input 
	  type="text"
	  class="form-control" path="article.name" value="${ article.name }" placeholder="Nom" />
  </div>

  <div class="form-group">
    <form:label path="article.price">Prix</form:label>
    <form:input 
	  type="number"
	  class="form-control" path="article.price" placeholder="Prix" value="${ article.price }"/>
  </div>

  <div class="form-group">
    <form:label path="article.categorie">Cat&eacute;gorie</form:label>
    <form:select path="article.categorie"> 
       <c:forEach var="cat" items="${ categories }" >
       		<form:option value="${ cat }">${ cat }</form:option>
       </c:forEach>
    </form:select>
  </div>

  

  <div class="form-group">
    <form:label path="lot">Lot</form:label>
    
    <c:choose>
	<c:when test="${modeVar == 'edit'}">
	      <form:input 
	  type="number"
	  class="form-control" path="lot" placeholder="Lot" value="${ lot }" disabled="true" />
	</c:when>
	
	<c:otherwise>
	      <form:input 
	  type="number"
	  class="form-control" path="lot" placeholder="Lot" value="${ lot }"/>
	</c:otherwise>
	</c:choose>   
    
  </div>
 
 
  <div class="form-group">
  	<form:label for="date_limite" path="date_limite"> Date limite </form:label>
  	<fmt:formatDate value="${ article.date_limite }" var="date_limite" pattern="yyyy-MM-dd" />
  	<form:input type="date" path="date_limite" class= "date" name = "date_limite" value="${article.date_limite}"/>
  </div>
 
  
  <div class="form-group">
  	<form:label path="quantite_recu">Quantit&eacute; re&ccedil;u</form:label>
  	
  	<c:choose>
	  	<c:when test="${modeVar == 'edit'}">
	  	<form:input 
		  type="number"
		  class="form-control" path="quantite_recu" placeholder="Quantit&eacute; re&ccedil;u"  value="${ quantite_recu }" disabled="true"/>
	  	
	  	</c:when>
	  	
	  	<c:otherwise>
	  	<form:input 
		  type="number"
		  class="form-control" path="quantite_recu" placeholder="Quantit&eacute; re&ccedil;u"  value="${ quantite_recu }"/>
	  	</c:otherwise>
  	</c:choose>
  	
  </div>
  
  <c:if test="${modeVar == 'edit'}">
  <div class="form-group">
    <form:label path="stock_p">Stock de l'article perissable</form:label>
    <form:input 
	  type="number"
	  class="form-control" path="stock_p" placeholder="Stock" value="${stock_p}"/>
  </div>
  
  <div class="form-group">
    <form:label path="article.stock">Stock total</form:label>
    <form:input 
	  type="number"
	  class="form-control" path="article.stock" placeholder="Stock" value="${ article.stock }" disabled="true"/>
  </div>
  </c:if>
  
  
  <c:if test="${messageErreur != ''}">
	<h2>${messageErreur}</h2>
  </c:if>
  
  <button type="submit" class="btn btn-primary">
    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
    Ok
  </button>
  
</form:form>
</div>
<%@include file="./_footer.jsp" %>

