<%@include file="./_header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


        <div class="container">
             <h1>Modification article : ${id} </h1>
             <form:form action="/stock/article/edit/${id}.html" method="post" modelAttribute="article">
                 <div class="form-row">
                     <div class="col-md-8 mb-8">
                         <form:label path="id">R&eacute;f&eacute;rence</form:label>
                         <form:input type="text" class="form-control" path="id" placeholder="Enter id" value="${ id }" disabled="true" />
                         <c:if test="${erreurId != ''}"> <div id="erreurId" class="col-12 messages" style="color: red; font-size: 1.1rem;">${erreurId}</div></c:if>
                     </div>
                     <div class="col-md-8 mb-8">
                         <form:label path="name">Name</form:label>
                         <form:input type="text" class="form-control" path="name" placeholder="Enter name" value="${ name }" />
                          <c:if test="${erreurName != ''}"> <div id="erreurId" class="col-12 messages" style="color: red; font-size: 1.1rem;">${erreurName}</div></c:if>
                     </div>
                     <div class="col-md-8 mb-8">
                         <form:label path="price">Prix</form:label>
                         <form:input type="number" class="form-control" path="price" placeholder="price" value="${ price }" />
                         <c:if test="${erreurPrice != ''}"> <div id="erreurId" class="col-12 messages" style="color: red; font-size: 1.1rem;">${erreurPrice}</div></c:if>
                     </div>
                 </div>
                 <br><br>
                 <div class="form-row">
                  <div class="col-md-8 mb-8">
                      <form:label  path="categorie">Categorie</form:label>
                   <form:select path="categorie" class="form-control">
                     <c:forEach var="categorie" items='${ categorie }'>
	                  <form:option value="${categorie}"> ${categorie} </form:option>
	                  </c:forEach>
	                  <c:if test="${erreurCategorie != ''}"> <div id="erreurId" class="col-12 messages" style="color: red; font-size: 1.1rem;">${erreurCategorie}</div></c:if>
                   </form:select>
                   </div>
                   <div id="erreurCategorie" class="col-12 messages" style="color: red; font-size: 1.1rem;"></div>

                     <div class="col-md-8 mb-8">
                         <form:label path="stock">Stock</form:label>
                         <form:input type="number" class="form-control" path="stock" placeholder="stock" value="${ stock }" />
                         <c:if test="${erreurStock != ''}"> <div id="erreurId" class="col-12 messages" style="color: red; font-size: 1.1rem;">${erreurStock}</div></c:if>
                     </div>
                 </div>
                 <br><br>
                 <div class="form-row">
                 <div class="col-md-8 mb-8">
                 <button class="btn btn-primary" type="submit">Ajouter</button>
                 </div>
                 </div>
             </form:form>
         </div>


<%@include file="./_footer.jsp" %>