<%@page import="fr.eservices.drive.model.Article"%>
<%@page import="java.util.List"%>
<%@page import="java.time.LocalDate"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="./_header.jsp"%>

<div class="container">
	<h2>La liste des articles</h2>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Nom</th>
					<th scope="col">Prix</th>
					<th scope="col">Categorie</th>
					<th scope="col">Nombre articles</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="produits" items='${ list_produits }'>
					<tr>
						<td>${ produits.id }</td>
						<td>${ produits.name }</td>
						<td>${ produits.price }</td>
						<td>${ produits.categorie }</td>
						<td>
							<form method="POST" action="/stock/panier.html">
								<input type="hidden" id="id" name="id" value="${produits.id}" />
								<input type="number" id="nb" name="nb" min="0"
									max="${produits.stock}" />
								<button type="submit" class="btn btn-primary">Ajouter</button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</div>

<%@include file="./commande_liste.jsp"%>

<form method="POST" action="/stock/commanderProduit.html">
	<button type="submit" class="btn btn-primary">Commander</button>
</form>
<br>

<form method="POST" action="/stock/annulerCommande.html">
	<button type="submit" class="btn btn-primary">Annuler</button>
</form>


<%@include file="./_footer.jsp"%>