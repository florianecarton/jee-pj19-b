<link rel=stylesheet type="text/css" href="css/perissable.css">
<%@include file="./_header.jsp" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<div class="col-md-8 col-md-offset-2" background-color: #fff">
<h2>
  Articles p&eacute;rissables
</h2>

<form action="/stock/perissable/new.html">
         <button type="submit">Ajouter un article p&eacute;rissable</button>
      </form>

<table class="table table-bordered table-striped table-hover">
  <tr>
	<th>R&eacute;f&eacute;rence</th>
	<th>Nom</th>
	<th>Prix</th>
	<th>Cat&eacute;gorie</th>
	<th>Date limite</th>
	<th>Lot</th>
	<th>Quantit&eacute; Re&ccedil;ue</th>
	<th>Stock restant</th>
	<th>Stock total</th>
	<th></th>

  </tr>
  
  <c:forEach var="perissable" items='${ articles }'>
  	  <tr>
		<td>${ perissable.article.id }</td>
		<td>${ perissable.article.name }</td>
		<td>${ perissable.article.price }</td>
		<td>${ perissable.article.categorie }</td>
		<td>${ perissable.date_limite }</td>
		<td>${ perissable.lot }</td>
		<td>${ perissable.quantite_recu }</td>
		<td>${ perissable.stock_p }</td>
		<td>${ perissable.article.stock }</td>
		<td>
			<form action="/stock/perissable/edit/${perissable.article.id }/${perissable.lot}.html" method="get">
	         	<button type="submit">Modifier</button>
	      	</form>
		</td>
  </tr>
  </c:forEach>
</table>
</div>

<!-- TODO : Select pour filtres et pagination -->

<%@include file="./_footer.jsp" %>

