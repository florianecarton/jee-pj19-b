<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<pre>
<c:choose>

	<c:when test="empty cart.getArticles()">
		aucun article
	</c:when>
	
	<c:otherwise>
		<c:forEach items="${ cart.getArticles() }" var="article">
		${article.getName() } - <fmt:formatNumber value="${article.getPrice() / 100}" type="currency"/>
		</c:forEach>
	</c:otherwise>
</c:choose>
</pre>
<a class="btn btn-primary" href="cart/1/validate.html">Commander</a>