<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@include file="./_header.jsp" %>
<div class="col-md-10 col-md-offset-2" style="border : solid 1px #666;background-color: #fff">
<h2>
 <span class="glyphicon glyphicon-gift" aria-hidden="true"> </span> 
 liste des Articles
 <form action="/stock/article/new.html">
         <button type="submit" class="btn btn-default" >Ajouter un article</button>
      </form>
</h2>
<div class="row" style="margin:5px">
<form class="form-inline">
<div class="form-group">
<label for="fCategorie">categorie</label>
 <select class="form-control" id="fCategorie" name="categorie">
  <option value="">tous</option>
	 <c:forEach var="categorie" items='${ categorie }'>
	    <option> ${categorie} </option>
	  </c:forEach>
</select>
</div>
<div class="form-group">
<label for="fName">name</label>
 <input type="text" class="form-control" id="fName"  name="name" placeholder="Enter Name" value="${ name }" >
</div>
<div class="form-group">
<label for="fId">Reference</label>
 <input type="text" class="form-control" id=fId  name="id" placeholder="Enter Reference" value="${ id }" >
</div>
<div class="form-group">
<label for="fPagination">pagin� par</label>
 <select class="form-control" id="fPagination" name="pagination">
  <option value="10">10</option>
  <option value="50">50</option>
  <option value="200">200</option>
</select>
</div>
<button type="submit" class="btn btn-default">chercher</button>
</form> 
</div>

<table class="table table-bordered table-striped table-hover">
  <tr>
	<th>R&eacute;f&eacute;rence</th>
	<th>Nom</th>
	<th>Prix</th>
	<th>Cat&eacute;gorie</th>
	<th>Stock</th>
  </tr>
  
  <c:forEach var="article" items='${ list_article.content }'>
  	  <tr>
		<td><a href="/stock/article/edit/${article.id }.html">${ article.id }</a></td>
		<td>${ article.name }</td>
		<td>${ article.price }</td>
		<td>${ article.categorie }</td>
		<td>${ article.stock }</td>
  </tr>
  </c:forEach>
</table>

<nav aria-label="page navigation">
   <ul class="pagination">
     <li>
        <a href="?page=1&categorie=${param.categorie}" aria-label="Debut"></a>
        </li>  
         <c:forEach var="i" begin="${list_article.number-10 >0 ? list_article.number-10 : 1 }"
                    end="${list_article.number+10 <list_article.totalPages ? list_article.number+10 : list_article.totalPages}">
                    <li class="$ { i == article_list.number+1 ? 'active' : ''}"><a href="?page=${i}&categorie=${param.categorie}">${i}</a></li>
         </c:forEach>
         <li>
         <a href="?page=${list_article.totalPages}&categorie=${param.categorie}" aria-label="Fin"></a>
         </li>
   </ul>
</nav>
</div>

<%@include file="./_footer.jsp" %>
