# JEE-PJ19-B

Repository for the last JEE project with M. Dufrene.

_Group B :_ Floriane CARTON, Marie GALLARDO, Mohamed Bakkari, Ayoub Lafquih

Nous avons choisit de lancer Tomcat depuis Eclipse afin que nous puissions tous utiliser les mêmes url et d'éviter les problèmes d'environnement.
Pour l'installation il suffit donc de créer un serveur 'MavenBuild' et d'ajouter dans le goal : `clean install -DskipTests tomcat7:run`

Le lien de l'application est donc : http://localhost:8080/stock/sample_products.jsp

Choix particuliers :

  - Nous avons choisis d'enlever l'objet Categorie de la base de données pour pouvoir se concentrer sur les stocks qui nous intéressaient d'avantage.
  - Nous avons choisis de ne pas utiliser de Dao car nos fonctionnalités/requêtes vers la base de données sont basiques et nous avons pu nous contenter du Repository.

Difficultés :

 - Nous avons eue des difficultés avec l'intégration de la base de données.
 - Merger nos différents travaux en un.
 - Marie : intégration et configuration de Tomcat pour que tout le monde puisse utiliser le même url
 - Marie : Date_limite
 - Floriane : Avoir un projet qui marche pour tous le monde a été compliqué
 - Floriane : Utiliser efficacement les Controller avec les views JSP
 - Floriane : Avoir la même vision sur l'aspect final du projet et le fait que chaque fonctionnalité était un peu liée avec les autres
